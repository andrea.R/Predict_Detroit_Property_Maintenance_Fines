# Predict Detroit Property Maintenance Fines
This challange is based on a data challenge from the Michigan Data Science Team.  
The first step in answering this question is understanding when and why a resident might fail 
to comply with a blight ticket. This is where predictive modeling comes in.
For this assignment, your task is to predict whether a given blight ticket will be paid on time.  
All tickets where the violators were found not responsible are not considered during evaluation.
They are included in the training set as an additional source of data for visualization,
and to enable unsupervised and semi-supervised approaches. However, they are not included in the test set.  
File descriptions:
 - train.csv - the training set (all tickets issued 2004-2011),
 - test.csv - the test set (all tickets issued 2012-2016),  

## Data fields
**train.csv & test.csv**  
 - ticket_id - unique identifier for tickets,
 - agency_name - Agency that issued the ticket,
 - inspector_name - Name of inspector that issued the ticket,
 - violator_name - Name of the person/organization that the ticket was issued to,
 - violation_street_number, violation_street_name, violation_zip_code - Address where the violation occurred,
 - mailing_address_str_number, mailing_address_str_name, city, state, zip_code, non_us_str_code, country - Mailing address of the violator,
 - ticket_issued_date - Date and time the ticket was issued,
 - hearing_date - Date and time the violator's hearing was scheduled,
 - violation_code, violation_description - Type of violation,
 - disposition - Judgment and judgement type,
 - fine_amount - Violation fine amount, excluding fees,
 - admin_fee - $20 fee assigned to responsible judgments,
 - state_fee - $10 fee assigned to responsible judgments,
 - late_fee - 10% fee assigned to responsible judgments,
 - discount_amount - discount applied, if any,
 - clean_up_cost - DPW clean-up or graffiti removal cost,
 - judgment_amount - Sum of all fines and fees,
 - grafitti_status - Flag for graffiti violations,  

**train.csv only**
 - payment_amount - Amount paid, if any,
 - payment_date - Date payment was made, if it was received,
 - payment_status - Current payment status as of Feb 1 2017,
 - balance_due - Fines and fees still owed,
 - collection_status - Flag for payments in collections,
 - **compliance [target variable for prediction]**:
    -  Null = Not responsible,
    -  0 = Responsible, non-compliant,
    -  1 = Responsible, compliant,
 - compliance_detail - More information on why each ticket was marked compliant or non-compliant,

## Goal
Your predictions will be given as the probability that the corresponding blight ticket will be paid on time.  
The evaluation metric for this assignment is the Area Under the ROC Curve (AUC).  
Your grade will be based on the AUC score computed for your classifier. A model which with an AUROC of 0.7 passes this assignment, over 0.75 will recieve full points.
